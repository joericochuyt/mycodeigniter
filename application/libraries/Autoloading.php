<?php
/**
 * Autoloader extension for CodeIgniter.
 * registers a static function in the spl_autoloader.
 * 
 * @author      Michael Rüfenacht
 * @version     0.3
 * @todo        think about a good solution for avoiding the loading of CI files
 *              f.e. the model
 * @todo        implement a variant with an iterator apply call
 */
class Autoloading
{
    /**
     * Folders that will be searched within.
     * @var array
     */
    public static $FOLDERS = array();

    /**
     * Index of already found files.
     * @var array associative $classname => $path
     */
    public static $INDEX = array();
    
    /**
     * Autoloading
     * <p>
     * Allows autoloading of php classes but needs some naming conventions. The
     * corresponding files need to be named like the class. Autoloading allows
     * to register rootpath and folders within, that should be browsed. When
     * no rootpath is specified the whole default root directory gets searched
     * recursively.
     * <p>
     * By default the autoloading indexes all files, so there's only one access
     * to the filesystem of the server.
     */
    public function  __construct()
    {
        //echo "called";
        // these functions should be called from outside.
        self::append_folder(array('models','controllers','libraries'),APPPATH);
        self::append_folder(array('libraries'),BASEPATH);
        self::initialize();
    }
    /**
     * If there's no possibility to call the constructor, the initialize function
     * should be called.
     */
    public static function initialize($folders = array())
    {
        // define file extension if codeigniter isnt used.
        if(!defined('EXT')) { define('EXT','.php'); }
        if(empty(self::$INDEX)) {
            // index the specified folders.
            self::create_index();
        }
        // register the autoloader
        spl_autoload_register('Autoloading::auto_load');
    }
    /**
     * Appends folders to the searchpaths, folders that are not bound to a root
     * path will be treatet as root folder.
     * 
     * @param string $folder
     * @param string $rootPath
     */
    public static function append_folder($folders, $rootPath = false)
    {
        $folders = (is_array($folders)&&!empty($folders))?$folders:array($folders);
        foreach($folders as $folder)
        {
            if($rootPath)
            {
                self::$FOLDERS[rtrim($rootPath, '/').'/'][] = $folder;
            }
            else
            {
                $folder = rtrim($folder, '/').'/';
                self::$FOLDERS[$folder] = (!isset(self::$FOLDERS[$folder]))
                                            ?array('')
                                            :self::$FOLDERS[$folder];
            }
        }
    }
    /**
     * Starts creating the fileindex.
     * <p>
     * Iterates over rootpaths and specified folders (even if they don't exist
     * in the root folder).
     */
    public static function create_index()
    {
        self::$FOLDERS = (empty(self::$FOLDERS))
                            ?array('./'=>array(''))
                            :self::$FOLDERS;
        foreach(self::$FOLDERS as $rootpath => $folders)
        {
            foreach($folders as $folderpath)
            {
                self::directory_index(realpath(rtrim($rootpath,'/').'/'.rtrim($folderpath,'/').'/'));
            }
        }
    }
    /*public static function index_file(Iterator $it)
    {
        $file = $it->current();
        if($file->getBasename(EXT)==$file->getBasename())
        {
            self::$INDEX[strtolower($file->getFilename())] = $file->getPathname();
        }
        return true;
    }*/
    /**
     * Creates the index of files in the specified directories (recursive).
     * @param string $dirpath
     */
    public static function directory_index($dirpath)
    {
        try
        {
            //$dirIterator = new DirectoryIterator($dirpath);
            $dirIterator =  new RecursiveIteratorIterator(
                                new RecursiveDirectoryIterator($dirpath)
                            );
            //iterator_apply($dirIterator, 'Autoloading::index_file',array($dirIterator));*/
            foreach($dirIterator as $directory)
            {
                if($directory->isFile())
                {
                    self::$INDEX[strtolower($directory->getFilename())] = $directory->getPathname();
                }
                /*if($directory->isDir()&&!$directory->isDot())
                {
                    self::directory_index($directory->getPathname());
                }*/
            }
        }
        catch (UnexpectedValueException $ex)
        {
            //var_dump($ex->getMessage());
        }
    }
    /**
     * Autoloading function.
     * @param string $classname
     */
    public static function auto_load($classname)
    {
        $classname = strtolower($classname).EXT;
        if(isset(self::$INDEX[$classname]))
        {
            require_once(self::$INDEX[$classname]);
        }
    }
}
?>
