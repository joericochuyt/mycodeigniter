<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * The Sandbox is a class to test several methods from the model.
 *
 * @author fabianschneider
 */
class Sandbox extends CI_Controller {

    public function index() {
        $this->load->view('foobar.php');
    }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */